import struct, os

class Bmp_Header(object):
        def __init__(self):
                self.dataOffset = 0
                self.width = 0
                self.height = 0
                self.bpp = 0
                self.dataSize = 0

        def isBMP(self):
                if struct.unpack('<2s', self.bmp.read(2))[0] != 'BM':
                        print("Not a bitmap. Exiting...")
                        raise exit(1)

        def isRLE(self):
                if struct.unpack('<I', self.bmp.read(4))[0] != 0:
                        print("RLE encoded bitmap. Not implemented.")
                        raise exit(1)

        def readHeader(self):
                self.isBMP() # May exit
                self.bmp.seek(10)
                self.dataOffset = struct.unpack('<I', self.bmp.read(4))[0]
                self.bmp.seek(18)
                self.width = struct.unpack('<I', self.bmp.read(4))[0]
                self.height = struct.unpack('<I', self.bmp.read(4))[0]
                self.bmp.seek(28)
                self.bpp = struct.unpack('<h', self.bmp.read(2))[0]
                if self.bpp != 24:
                        print("Bitmap's bit depth is not currently supported.")
                        raise exit(1)
                self.isRLE() # May exit
                self.dataSize = struct.unpack('<I', self.bmp.read(4))[0]

        def printHeaderData(self):
                print("File Size: %i" % self.size)
                print("Image Data offset: %i" % self.dataOffset)
                print("Image Width: %i" % self.width)
                print("Image Height: %i" % self.height)
                print("Image Bit depth: %i" % self.bpp)
                print("Image Data Size: %i" % self.dataSize)


class Bmp(Bmp_Header):
        def __init__(self, bmp):
                self.bmp = open(bmp, 'rb')
                self.size = os.path.getsize(bmp)
                self.padLeng = 0
                self.pixArray = []

        def readBmp24(self):
                self.bmp.seek(self.dataOffset)

                # set buffer to bytes in a row
                buffSize = ((self.bpp * self.width + 31) / 32) * 4

                for row in range(self.height):
                        temp = []
                        buff = self.bmp.read(buffSize)
                        offset = 0
                        end = 3
                        while (offset / 3) < self.width:
                                temp.append(struct.unpack('<I', buff[offset:end] + '\0')[0])
                                offset += 3
                                end += 3
                        self.pixArray.insert(0, temp)

        def printBmp(self):
                trans = {16777215: ' ', 16711680: '%', 255: '@', 0: '#'}
                for row in self.pixArray:
                        s = ""
                        for pixel in row:
                                s += trans[pixel]
                        print s

        def loadBmp(self):
                self.readHeader()
                if self.bpp == 24:
                        self.readBmp24()
                else:
                        print("Bitmap's bit depth is not currently supported.")
                        raise exit(1)

        def getData(self):
                return self.pixArray[:]

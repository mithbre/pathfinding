import bmp

def simplifyMaze(maze):
        trans = {16777215: 0, 16711680: 1, 255: 2, 0: -1}
        # shorten the numbers a bit
        for row in range(len(maze)):
                for column in range(len(maze[0])):
                        maze[row][column] = trans[maze[row][column]]

def getStartPoint(maze):
        x = -1
        y = -1

        for i in range(len(maze)):
                try:
                        x = maze[i].index(1)
                        y = i
                        break
                except:
                        continue

        if x == -1 or y == -1:
                print("No starting point.")
                exit(1)

        return x, y

def legalMoves(maze, x, y):
        legal = {'north': 0, 'east': 0, 'south': 0, 'west': 0}
        if maze[y-1][x] == 0: # North
                legal['north'] = 1
        if maze[y][x+1] == 0: # East
                legal['east'] = 1
        if maze[y+1][x] == 0: # South
                legal['south'] = 1
        if maze[y][x-1] == 0: # West
                legal['west'] = 1
        return legal


image = bmp.Bmp("./maze1.bmp")
image.loadBmp()
maze = image.getData()
simplifyMaze(maze)
x, y = getStartPoint(maze)
moves = legalMoves(maze, x, y)
